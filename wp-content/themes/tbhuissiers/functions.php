<?php

    // Chargement des styles et des scripts Bootstrap sur WordPress
    function tbhuissiers_styles_scripts(){
        // Styles
        wp_enqueue_style('tbhuissiers-owlcarousel-style', get_template_directory_uri() . '/lib/owlcarousel/owl.carousel.min.css');
        wp_enqueue_style('tbhuissiers-owltheme-style', get_template_directory_uri() . '/lib/owlcarousel/owl.theme.default.min.css');
        wp_enqueue_style('tbhuissiers-bootstrap-style', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css');
        wp_enqueue_style('custom-style', get_stylesheet_uri());
        // Scripts
        wp_enqueue_script('tbhuissiers-owlcarousel', get_template_directory_uri() . '/lib/owlcarousel/owl.carousel.min.js');
        wp_enqueue_script('tbhuissiers-bootstrap-jquery', 'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js');
        wp_enqueue_script('tbhuissiers-bootstrap-popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js');
        wp_enqueue_script('tbhuissiers-bootstrap-script', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js');
        wp_enqueue_script('custom-script', get_template_directory_uri() . '/js/main.js');
    }
    add_action('wp_enqueue_scripts', 'tbhuissiers_styles_scripts');


    // Ajout de points d'accroche de menus
    function tbhuissiers_after_setup_theme() {
        // On ajoute des menus
        register_nav_menu('menu_principal', "Menu principal");
        register_nav_menu('menu_footer_navigation', "Footer - Navigation");
        // On ajoute une classe php permettant de gérer les menus Bootstrap
        require_once get_template_directory() . '/lib/bootstrap/class-wp-bootstrap-navwalker.php';
    }
    add_action('after_setup_theme', 'tbhuissiers_after_setup_theme');


    // Affiche la page Options de ACF Pro
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }


    // Ajout des images pour les actus
    add_theme_support( 'post-thumbnails' );


    // Ajout d'une taille d'image pour les actus
    // add_image_size( 'taille-exemple', 700, 835, array( 'center', 'center' ));


    // Suppression accès page auteur
    function tbhuissiers_disable_author_page() {
        global $wp_query;

        if ( is_author() ) {
            // Redirect to homepage
            wp_redirect(get_option('home'));
        }
    }
    add_action( 'template_redirect', 'tbhuissiers_disable_author_page' );


    function custom_pre_get_posts($query) {
        if ($query->is_main_query() && !$query->is_feed() && !is_admin() && is_category()) {
            $query->set('page_val', get_query_var('paged'));
            $query->set('paged', 0);
        }
    }

    add_action('pre_get_posts', 'custom_pre_get_posts');

    // function custom_tag_pre_get_posts($query) {
    //     if ($query->is_main_query() && !$query->is_feed() && !is_admin() && is_tag()) {
    //         $query->set('page_val', get_query_var('paged'));
    //         $query->set('paged', 0);
    //     }
    // }

    // add_action('pre_get_posts', 'custom_tag_pre_get_posts');

    
?>