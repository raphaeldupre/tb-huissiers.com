<?php get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">

			<div class="base-page-content-wrapper">
				<div class="container">

					<?php 
						$paged = (get_query_var( 'page_val' )) ? get_query_var( 'page_val' ) : 1;
						$args = array(
							'post_type'      	=> 'post',
							'posts_per_page' 	=> 14,
							'orderby' 		 	=> 'date',
							'post_status'    	=> 'publish',
							'paged' => $paged,
							'page' 		 => $paged,
						);
						$query = new WP_Query( $args );
						
						if ( $query->have_posts() ) {
							echo '<div class="actus-liste-global">';

								while ( $query->have_posts() ) { 
									$query->the_post();
									$categories = get_the_category();

									echo get_the_title();
									echo get_permalink();
									// echo the_post_thumbnail('taille-exemple');
									echo get_the_time('d F Y');
								}

								echo '<div class="post-pager">';
									$big = 999999999;
									echo paginate_links(array(
										'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
										'format' => '/page/%#%',
										'show_all' => false,
										'mid_size' => 0,
										'end_size' => 1,
										'current' => max(1, $paged),
										'prev_text' => '<img src="/wp-content/themes/THEME_NAME/images/IMG.png">',
										'next_text' => '<img src="/wp-content/themes/THEME_NAME/images/IMG.png">',
										'total' => $query->max_num_pages
									));
								echo '</div>';

								wp_reset_postdata();

							echo '</div>';
							
						} else {
							echo '<p class="no-content">Aucun contenu ne correspond à votre recherche</p>';
						}
					?>
					
				</div>
			</div>

		</div><!-- #main -->
	</section><!-- #primary -->
	
<?php get_footer(); ?>