<?php get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">

			<div class="base-page-content-wrapper">

				<div class="container">
					<div class="base-page-content row">

						<?php while ( have_posts() ) : the_post(); ?>
							<div class="page-title-wrapper col-lg-9 col-md-12">
								<div class="page-title-content">
									<h1><?php the_title(); ?></h1>
									<p><?php echo get_the_time('F Y'); ?></p>
								</div>
							</div>
						<?php endwhile; ?>

						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
							<?php echo the_post_thumbnail('taille-exemple'); ?>
						<?php endwhile; ?>

					</div>
				</div>

			</div>

		</div><!-- #main -->
	</section><!-- #primary -->
	
<?php get_footer(); ?>